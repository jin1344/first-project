package test.project.first.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import test.project.first.userDAO.UserDAO;
import test.project.first.userVO.UserInfo;


@Controller
public class UserController {
	
	@Autowired
	UserDAO dao = new UserDAO();
	
	
	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public String signUp(UserInfo ur, Model model) {

		int result = dao.insertUser(ur);
		if (result == 1) {
			return "redirect:/";
		} else {
			return "signUp";
		}
	}
	
		
	@RequestMapping(value = "/idCheck", method = RequestMethod.POST)
	public @ResponseBody String loginAction(String userId) {
		
		if (userId.length() < 3) {
			return "fail";
		}
		
		return "success";
	}

}
