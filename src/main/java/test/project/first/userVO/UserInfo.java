package test.project.first.userVO;

public class UserInfo { 
	
	public String userId;
	public String userPw;
	public String name;
	public String birthDate;
	public String gender;
	public String phone;
	public String address;
	public String interest1;
	public String interest2;
	public String lv;
	public String email;
	public String cash;
	public String totalTerm;
	public String totalAttendance;
	
	public UserInfo(String userId, String userPw, String name, String birthDate, String gender, String phone,
			String address, String interest1, String interest2, String lv, String email, String cash, String totalTerm,
			String totalAttendance) {
		super();
		this.userId = userId;
		this.userPw = userPw;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.phone = phone;
		this.address = address;
		this.interest1 = interest1;
		this.interest2 = interest2;
		this.lv = lv;
		this.email = email;
		this.cash = cash;
		this.totalTerm = totalTerm;
		this.totalAttendance = totalAttendance;
	}
	public UserInfo() {
		super();
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPw() {
		return userPw;
	}
	public void setUserPw(String userPw) {
		this.userPw = userPw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getInterest1() {
		return interest1;
	}
	public void setInterest1(String interest1) {
		this.interest1 = interest1;
	}
	public String getInterest2() {
		return interest2;
	}
	public void setInterest2(String interest2) {
		this.interest2 = interest2;
	}
	public String getLv() {
		return lv;
	}
	public void setLv(String lv) {
		this.lv = lv;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCash() {
		return cash;
	}
	public void setCash(String cash) {
		this.cash = cash;
	}
	public String getTotalTerm() {
		return totalTerm;
	}
	public void setTotalTerm(String totalTerm) {
		this.totalTerm = totalTerm;
	}
	public String getTotalAttendance() {
		return totalAttendance;
	}
	public void setTotalAttendance(String totalAttendance) {
		this.totalAttendance = totalAttendance;
	}
	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", userPw=" + userPw + ", name=" + name + ", birthDate=" + birthDate
				+ ", gender=" + gender + ", phone=" + phone + ", address=" + address + ", interest1=" + interest1
				+ ", interest2=" + interest2 + ", lv=" + lv + ", email=" + email + ", cash=" + cash + ", totalTerm="
				+ totalTerm + ", totalAttendance=" + totalAttendance + "]";
	}
	
	

}
