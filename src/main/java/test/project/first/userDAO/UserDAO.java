package test.project.first.userDAO;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import test.project.first.userVO.UserInfo;



@Repository
public class UserDAO {
	
	@Autowired
	SqlSession session;
	
	public int insertUser(UserInfo ur) {
		
		int result = 0;		

		UserMapper mapper = session.getMapper(UserMapper.class);

		try {
			result = mapper.insertUser(ur);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}

		return result;
	}

}
