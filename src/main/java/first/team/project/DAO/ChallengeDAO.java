package first.team.project.DAO;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import first.team.project.VO.Challenger;
import first.team.project.VO.Exam;

@Repository
public class ChallengeDAO {

	@Autowired
	SqlSession session;
	
	public ArrayList<Exam> selectChallengeExam(String now) {
		ArrayList<Exam> result = null;
		
		ChallengeMapper mapper = session.getMapper(ChallengeMapper.class);
		
		try {
		result = mapper.selectChallengeExam(now);
		}catch(Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public Exam detailChallenge(int examseq) {
		Exam result = null;
		ChallengeMapper mapper = session.getMapper(ChallengeMapper.class);
		try {
			result = mapper.detailChallenge(examseq);
		}catch(Exception e) {
			return result;
		}
		return result;
	}
	
	public int insertChallenge(Challenger challenger) {
		int result = 0;
		ChallengeMapper mapper = session.getMapper(ChallengeMapper.class);
		try {
			result = mapper.insertChallenge(challenger);
		}catch(Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
}
