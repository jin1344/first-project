package first.team.project.DAO;

import java.util.ArrayList;

import first.team.project.VO.Challenger;
import first.team.project.VO.Exam;

public interface ChallengeMapper {

	public ArrayList<Exam> selectChallengeExam(String now); //DB에서 첼린저 리스트가져옴
	
	public Exam detailChallenge(int examseq); //시퀀스값으로 상새 첼린지 정보 가져옴
	
	public int insertChallenge(Challenger challenger);//유저의 첼린지정보를 DB에 등록
	
}
