package first.team.project.VO;

public class Challenger {
	
	public int examseq; 
	public String userid; 
	public int entryfee; 
	public String situation;
	
	public Challenger() {
		super();
	}

	public Challenger(int examseq, String userid, int entryfee, String situation) {
		super();
		this.examseq = examseq;
		this.userid = userid;
		this.entryfee = entryfee;
		this.situation = situation;
	}

	public int getExamseq() {
		return examseq;
	}

	public void setExamseq(int examseq) {
		this.examseq = examseq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getEntryfee() {
		return entryfee;
	}

	public void setEntryfee(int entryfee) {
		this.entryfee = entryfee;
	}

	public String getSituation() {
		return situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

	@Override
	public String toString() {
		return "Challenger [examseq=" + examseq + ", userid=" + userid + ", entryfee=" + entryfee + ", situation="
				+ situation + "]";
	} 

	

}
