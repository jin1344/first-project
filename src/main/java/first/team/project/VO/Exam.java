package first.team.project.VO;

public class Exam {
	
	 private int examseq;
     private String name; 
     private String examdate; 
     private int invoice; 
     private int totalcash; 
     
	public Exam() {
		super();
	}

	public Exam(int examseq, String name, String examdate, int invoice, int totalcash) {
		super();
		this.examseq = examseq;
		this.name = name;
		this.examdate = examdate;
		this.invoice = invoice;
		this.totalcash = totalcash;
	}

	public int getExamseq() {
		return examseq;
	}

	public void setExamseq(int examseq) {
		this.examseq = examseq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExamdate() {
		return examdate;
	}

	public void setExamdate(String examdate) {
		this.examdate = examdate;
	}

	public int getInvoice() {
		return invoice;
	}

	public void setInvoice(int invoice) {
		this.invoice = invoice;
	}

	public int getTotalcash() {
		return totalcash;
	}

	public void setTotalcash(int totalcash) {
		this.totalcash = totalcash;
	}

	@Override
	public String toString() {
		return "Exam [examseq=" + examseq + ", name=" + name + ", examdate=" + examdate + ", invoice=" + invoice
				+ ", totalcash=" + totalcash + "]";
	}
	
}

	