package first.team.project.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import first.team.project.DAO.ChallengeDAO;
import first.team.project.VO.Challenger;
import first.team.project.VO.Exam;

@Controller
public class ChallengeController {

	@Autowired
	ChallengeDAO dao;
	
	//첼린지 리스트를 DB에서 가져옴
	@ResponseBody
	@RequestMapping(value = "/challengeList", method = RequestMethod.GET)
	public ArrayList<Exam> selectChallengeExam(String now) {		
		ArrayList<Exam> challengeList = null;
		
		challengeList = dao.selectChallengeExam(now);
				 
		return challengeList;
	}
	
	//선택한 첼린지의 상세정보를 보러감
	@RequestMapping(value = "/detailChallenge", method = RequestMethod.GET)
	public String detailChallenge(int examseq, Model model) {		
		Exam result = null;
		result = dao.detailChallenge(examseq);
		
		model.addAttribute("exam",result);
		return "detailChallenge";
	}
	
	//첼린지 도전하면 금액을 DB에 등록하고 다시 첼린지달력화면으로 이동
	@RequestMapping(value = "/entryChallenge", method = RequestMethod.POST)
	public String entryChallenge(Challenger challenger) {		
	System.out.println(challenger);
	int result = dao.insertChallenge(challenger);
	System.out.println(result);
	if(result == 0) {
		return "detailChallenge";
	}
	return "challenge";
	}
		
}
