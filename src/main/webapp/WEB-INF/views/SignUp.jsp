<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>null</title>
<link rel='stylesheet' id='ticketlab-helpers-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/helpers.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel='stylesheet' id='main-bbt-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/bbt.css?ver=4.9.9'
	type='text/css' media='all' />
<!-- <link
	href="//maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"
	rel="stylesheet"> -->
<script type="text/javascript"
	src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript"
	src="//maxcdn.bootstrapcdn.com/bootstrap/latest/js/bootstrap.min.js"></script>
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	function sample6_execDaumPostcode() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var addr = ''; // 주소 변수
						var extraAddr = ''; // 참고항목 변수

						//사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							addr = data.roadAddress;
						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							addr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
						if (data.userSelectedType === 'R') {
							// 법정동명이 있을 경우 추가한다. (법정리는 제외)
							// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
							if (data.bname !== ''
									&& /[동|로|가]$/g.test(data.bname)) {
								extraAddr += data.bname;
							}
							// 건물명이 있고, 공동주택일 경우 추가한다.
							if (data.buildingName !== ''
									&& data.apartment === 'Y') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
							if (extraAddr !== '') {
								extraAddr = ' (' + extraAddr + ')';
							}
							// 조합된 참고항목을 해당 필드에 넣는다.
							document.getElementById("sample6_extraAddress").value = extraAddr;

						} else {
							document.getElementById("sample6_extraAddress").value = '';
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('sample6_postcode').value = data.zonecode;
						document.getElementById("sample6_address").value = addr;
						// 커서를 상세주소 필드로 이동한다.
						document.getElementById("sample6_detailAddress")
								.focus();
					}
				}).open();
	}
</script>
<style>

/* 상단 부분 스타일 */
.container {
	width: 100%;
	height: 15%;
	margin: 0px auto;
}

.divLeft {
	width: 15%;
	height: 70%;
	float: left;
	position: relative;
	left: 40px;
}

.divRight {
	width: 80%;
	height: 70%;
	float: right;
}

/* 상단 폰트 설정 */
.header_secondary .navbar-nav>li>a {
	color: #fff;
	font-size: 15px;
}

.header_secondary {
	background-color: #000 !important;
}

.header_secondary .second_menu {
	color: #fff !important;
}

.header_secondary .navbar-brand {
	padding-top: 15px !important
}

.title-center {
	position: relative;
	left: 43%;
}

.nav-title {
	font-size: 30px;
	/* 	color: blue; */
	list-style: none;
}

.liSet {
	font: strong 30px Dotum blue;
	padding: 20px 15px;
	font-style:
}

/* nav tag */
nav ul {
	padding-top: 10px;
} /* 상단 여백 10px */
nav ul li {
	display: inline; /* 세로나열을 가로나열로 변경 */
	border-left: 1px solid #999; /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
	font: bold 12px Dotum; /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
	padding: 0 10px; /* 각 메뉴 간격 */
}

nav ul li:first-child {
	border-left: none;
}

/
* 메뉴 분류중 제일 왼쪽의 "|"는 삭제


.root {
	width: 100%;
	height: 800px;
	background-color: #fff
}

.titleDiv {
	height: 20%;
	margin: 0px auto;
	border: 1px solid #bcbcbc;
}

.login {
	margin-left: 80%;
}

.divCenter {
	width: 66%;
	height: 70%;
	float: left;
}

.divRight {
	width: 40%;
	height: 70%;
	float: right;
}

.auth {w
	
}

.bar {
	letter-spacing: 10px;
}

.form-control {
	display: block;
	width: 100%;
	padding: .375rem .75rem;
	font-size: 1rem;
	line-height: 1.5;
	color: #495057;
	background-color: #fff;
	background-clip: padding-box;
	border: 1px solid #ced4da;
	border-radius: .25rem;
	transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

.form-control::-ms-expand {
	background-color: transparent;
	border: 0
}

.form-control:focus {
	color: #495057;
	background-color: #fff;
	border-color: #80bdff;
	outline: 0;
	box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25)
}

.form-control::-webkit-input-placeholder {
	color: #6c757d;
	opacity: 1
}

.form-control::-moz-placeholder {
	color: #6c757d;
	opacity: 1
}

.form-control:-ms-input-placeholder {
	color: #6c757d;
	opacity: 1
}

.form-control::-ms-input-placeholder {
	color: #6c757d;
	opacity: 1
}

.form-control::placeholder {
	color: #6c757d;
	opacity: 1
}

.form-control:disabled, .form-control[readonly] {
	background-color: #e9ecef;
	opacity: 1
}

select.form-control:not([size]):not([multiple]){height:calc(2.25rem+2px)}
select.form-control:focus::-ms-value {
	color: #495057;
	background-color: #fff
}
.form-control-file, .form-control-range {
	display: block;
	width: 100%
}

.col-form-label {
	padding-top: calc(.375rem + 1px);
	padding-bottom: calc(.375rem + 1px);
	margin-bottom: 0;
	font-size: inherit;
	line-height: 1.5
}

.col-form-label-lg {
	padding-top: calc(.5rem + 1px);
	padding-bottom: calc(.5rem + 1px);
	font-size: 1.25rem;
	line-height: 1.5
}

.col-form-label-sm {
	padding-top: calc(.25rem + 1px);
	padding-bottom: calc(.25rem + 1px);
	font-size: .875rem;
	line-height: 1.5
}

.form-control-plaintext {
	display: block;
	width: 100%;
	padding-top: .375rem;
	padding-bottom: .375rem;
	margin-bottom: 0;
	line-height: 1.5;
	background-color: transparent;
	border: solid transparent;
	border-width: 1px 0
}

.form-control-plaintext.form-control-lg, .form-control-plaintext.form-control-sm,
	.input-group-lg>.form-control-plaintext.form-control, .input-group-lg>.input-group-append>.form-control-plaintext.btn,
	.input-group-lg>.input-group-append>.form-control-plaintext.input-group-text,
	.input-group-lg>.input-group-prepend>.form-control-plaintext.btn,
	.input-group-lg>.input-group-prepend>.form-control-plaintext.input-group-text,
	.input-group-sm>.form-control-plaintext.form-control, .input-group-sm>.input-group-append>.form-control-plaintext.btn,
	.input-group-sm>.input-group-append>.form-control-plaintext.input-group-text,
	.input-group-sm>.input-group-prepend>.form-control-plaintext.btn,
	.input-group-sm>.input-group-prepend>.form-control-plaintext.input-group-text
	{
	padding-right: 0;
	padding-left: 0
}

.form-control-sm, .input-group-sm>.form-control, .input-group-sm>.input-group-append>.btn,
	.input-group-sm>.input-group-append>.input-group-text, .input-group-sm>.input-group-prepend>.btn,
	.input-group-sm>.input-group-prepend>.input-group-text {
	padding: .25rem .5rem;
	font-size: .875rem;
	line-height: 1.5;
	border-radius: .2rem
}

.input-group-sm>
.input-group-append>
select.btn:not([size]):not([multiple]),.input-group-sm>.input-group-append>
select.input-group-text:not([size]):not([multiple]),.input-group-sm>.input-group-prepend>
select.btn:not([size]):not([multiple]),.input-group-sm>.input-group-prepend>
select.input-group-text:not([size]):not([multiple]),.input-group-sm>
select.form-control:not([size]):not([multiple]),
select.form-control-sm:not([size]):not([multiple]){height:calc(1.8125rem+2px)}
.form-control-lg, .input-group-lg>.form-control, .input-group-lg>.input-group-append>.btn,
	.input-group-lg>.input-group-append>.input-group-text, .input-group-lg>.input-group-prepend>.btn,
	.input-group-lg>.input-group-prepend>.input-group-text {
	padding: .5rem 1rem;
	font-size: 1.25rem;
	line-height: 1.5;
	border-radius: .3rem
}
.input-group-lg>
.input-group-append>
select.btn:not([size]:not[multiple]),.input-group-lg>.input-group-append>
select.input-group-text:not([size]):not([multiple]),.input-group-lg>.input-group-prepend>
select.btn:not([size]):not([multiple]),.input-group-lg>.input-group-prepend>
select.input-group-text:not([size]):not([multiple]),.input-group-lg>
select.form-control:not([size]):not([multiple]),
select.form-control-lg:not([size]):not([multiple]){height:calc(2.875rem+2px)}
.form-group {
	margin-bottom: 1rem
}

.form-text {
	display: block;
	margin-top: .25rem
}

.form-row {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-ms-flex-wrap: wrap;
	flex-wrap: wrap;
	margin-right: -5px;
	margin-left: -5px
}

.form-row>.col, .form-row>[class*=col-] {
	padding-right: 5px;
	padding-left: 5px
}

.form-check {
	position: relative;
	display: block;
	padding-left: 1.25rem
}

.form-check-input {
	position: absolute;
	margin-top: .3rem;
	margin-left: -1.25rem
}

.form-check-input:disabled ~.form-check-label {
	color: #6c757d
}

.form-check-label {
	margin-bottom: 0
}

.form-check-inline {
	display: -webkit-inline-box;
	display: -ms-inline-flexbox;
	display: inline-flex;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	padding-left: 0;
	margin-right: .75rem
}

.form-check-inline .form-check-input {
	position: static;
	margin-top: 0;
	margin-right: .3125rem;
	margin-left: 0
}

.valid-feedback {
	display: none;
	width: 100%;
	margin-top: .25rem;
	font-size: 80%;
	color: #28a745
}

.valid-tooltip {
	position: absolute;
	top: 100%;
	z-index: 5;
	display: none;
	max-width: 100%;
	padding: .5rem;
	margin-top: .1rem;
	font-size: .875rem;
	line-height: 1;
	color: #fff;
	background-color: rgba(40, 167, 69, .8);
	border-radius: .2rem
}

.custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid,
	.was-validated .form-control:valid {
	border-color: #28a745
}

.custom-select.is-valid:focus, .form-control.is-valid:focus,
	.was-validated .custom-select:valid:focus, .was-validated .form-control:valid:focus
	{
	border-color: #28a745;
	box-shadow: 0 0 0 .2rem rgba(40, 167, 69, .25)
}

.custom-select.is-valid ~.valid-feedback, .custom-select.is-valid ~.valid-tooltip,
	.form-control.is-valid ~.valid-feedback, .form-control.is-valid ~.valid-tooltip,
	.was-validated .custom-select:valid ~.valid-feedback, .was-validated .custom-select:valid
	 ~.valid-tooltip, .was-validated .form-control:valid ~.valid-feedback,
	.was-validated .form-control:valid ~.valid-tooltip {
	display: block
}

.form-check-input.is-valid ~.form-check-label, .was-validated .form-check-input:valid
	 ~.form-check-label {
	color: #28a745
}

.form-check-input.is-valid ~.valid-feedback, .form-check-input.is-valid
	 ~.valid-tooltip, .was-validated .form-check-input:valid ~.valid-feedback,
	.was-validated .form-check-input:valid ~.valid-tooltip {
	display: block
}

.custom-control-input.is-valid ~.custom-control-label, .was-validated .custom-control-input:valid
	 ~.custom-control-label {
	color: #28a745
}

.custom-control-input.is-valid ~.custom-control-label::before,
	.was-validated .custom-control-input:valid ~.custom-control-label::before
	{
	background-color: #71dd8a
}

.custom-control-input.is-valid ~.valid-feedback, .custom-control-input.is-valid
	 ~.valid-tooltip, .was-validated .custom-control-input:valid ~.valid-feedback,
	.was-validated .custom-control-input:valid ~.valid-tooltip {
	display: block
}

.custom-control-input.is-valid:checked ~.custom-control-label::before,
	.was-validated .custom-control-input:valid:checked ~.custom-control-label::before
	{
	background-color: #34ce57
}

.custom-control-input.is-valid:focus ~.custom-control-label::before,
	.was-validated .custom-control-input:valid:focus ~.custom-control-label::before
	{
	box-shadow: 0 0 0 1px #fff, 0 0 0 .2rem rgba(40, 167, 69, .25)
}

.custom-file-input.is-valid ~.custom-file-label, .was-validated .custom-file-input:valid
	 ~.custom-file-label {
	border-color: #28a745
}

.custom-file-input.is-valid ~.custom-file-label::before, .was-validated .custom-file-input:valid
	 ~.custom-file-label::before {
	border-color: inherit
}

.custom-file-input.is-valid ~.valid-feedback, .custom-file-input.is-valid
	 ~.valid-tooltip, .was-validated .custom-file-input:valid ~.valid-feedback,
	.was-validated .custom-file-input:valid ~.valid-tooltip {
	display: block
}

.custom-file-input.is-valid:focus ~.custom-file-label, .was-validated .custom-file-input:valid:focus
	 ~.custom-file-label {
	box-shadow: 0 0 0 .2rem rgba(40, 167, 69, .25)
}

.invalid-feedback {
	display: none;
	width: 100%;
	margin-top: .25rem;
	font-size: 80%;
	color: #dc3545
}

.invalid-tooltip {
	position: absolute;
	top: 100%;
	z-index: 5;
	display: none;
	max-width: 100%;
	padding: .5rem;
	margin-top: .1rem;
	font-size: .875rem;
	line-height: 1;
	color: #fff;
	background-color: rgba(220, 53, 69, .8);
	border-radius: .2rem
}

.custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid,
	.was-validated .form-control:invalid {
	border-color: #dc3545
}

.custom-select.is-invalid:focus, .form-control.is-invalid:focus,
	.was-validated .custom-select:invalid:focus, .was-validated .form-control:invalid:focus
	{
	border-color: #dc3545;
	box-shadow: 0 0 0 .2rem rgba(220, 53, 69, .25)
}

.custom-select.is-invalid ~.invalid-feedback, .custom-select.is-invalid
	 ~.invalid-tooltip, .form-control.is-invalid ~.invalid-feedback,
	.form-control.is-invalid ~.invalid-tooltip, .was-validated .custom-select:invalid
	 ~.invalid-feedback, .was-validated .custom-select:invalid ~.invalid-tooltip,
	.was-validated .form-control:invalid ~.invalid-feedback, .was-validated .form-control:invalid
	 ~.invalid-tooltip {
	display: block
}

.form-check-input.is-invalid ~.form-check-label, .was-validated .form-check-input:invalid
	 ~.form-check-label {
	color: #dc3545
}

.form-check-input.is-invalid ~.invalid-feedback, .form-check-input.is-invalid
	 ~.invalid-tooltip, .was-validated .form-check-input:invalid ~.invalid-feedback,
	.was-validated .form-check-input:invalid ~.invalid-tooltip {
	display: block
}

.custom-control-input.is-invalid ~.custom-control-label, .was-validated .custom-control-input:invalid
	 ~.custom-control-label {
	color: #dc3545
}

.custom-control-input.is-invalid ~.custom-control-label::before,
	.was-validated .custom-control-input:invalid ~.custom-control-label::before
	{
	background-color: #efa2a9
}

.custom-control-input.is-invalid ~.invalid-feedback,
	.custom-control-input.is-invalid ~.invalid-tooltip, .was-validated .custom-control-input:invalid
	 ~.invalid-feedback, .was-validated .custom-control-input:invalid ~.invalid-tooltip
	{
	display: block
}

.custom-control-input.is-invalid:checked ~.custom-control-label::before,
	.was-validated .custom-control-input:invalid:checked ~.custom-control-label::before
	{
	background-color: #e4606d
}

.custom-control-input.is-invalid:focus ~.custom-control-label::before,
	.was-validated .custom-control-input:invalid:focus ~.custom-control-label::before
	{
	box-shadow: 0 0 0 1px #fff, 0 0 0 .2rem rgba(220, 53, 69, .25)
}

.custom-file-input.is-invalid ~.custom-file-label, .was-validated .custom-file-input:invalid
	 ~.custom-file-label {
	border-color: #dc3545
}

.custom-file-input.is-invalid ~.custom-file-label::before,
	.was-validated .custom-file-input:invalid ~.custom-file-label::before {
	border-color: inherit
}

.custom-file-input.is-invalid ~.invalid-feedback, .custom-file-input.is-invalid
	 ~.invalid-tooltip, .was-validated .custom-file-input:invalid ~.invalid-feedback,
	.was-validated .custom-file-input:invalid ~.invalid-tooltip {
	display: block
}

.custom-file-input.is-invalid:focus ~.custom-file-label, .was-validated .custom-file-input:invalid:focus
	 ~.custom-file-label {
	box-shadow: 0 0 0 .2rem rgba(220, 53, 69, .25)
}

.form-inline {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: horizontal;
	-webkit-box-direction: normal;
	-ms-flex-flow: row wrap;
	flex-flow: row wrap;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center
}

.form-inline .form-check {
	width: 100%
}

@media ( min-width :576px) {
	.form-inline label {
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		justify-content: center;
		margin-bottom: 0
	}
	.form-inline .form-group {
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-flex: 0;
		-ms-flex: 0 0 auto;
		flex: 0 0 auto;
		-webkit-box-orient: horizontal;
		-webkit-box-direction: normal;
		-ms-flex-flow: row wrap;
		flex-flow: row wrap;
		-webkit-box-align: center;
		-ms-flex-align: center;
		align-items: center;
		margin-bottom: 0
	}
	.form-inline .form-control {
		display: inline-block;
		width: auto;
		vertical-align: middle
	}
	.form-inline .form-control-plaintext {
		display: inline-block
	}
}

.signUpForm {
	width: 400px;
	height: 600px;
	position: relative;
	top: 100px;
	margin-left: auto;
	margin-right: auto;
	margin-left: auto;
}

.signLDiv {
	width: 30%;
	height: 70%;
	float: left;
	height: 70%;
}

.signRDiv {
	width: 70%;
	height: 70%;
	float: left;
}
</style>



<style type="text/css">
table {
	cellpadding: 0;
	cellspacing: 0;
	border-left: 0;
	border-right: 0;
	border-bottom: 0;
	border-top: 0;
}

td {
	cellpadding: 0;
	cellspacing: 0;
	border-left: 0;
	border-right: 0;
	border-bottom: 0;
	border-top: 0;
}

body {
	margin: 0;
	padding: 0;
	background-color: #F5F6F7;
}

a:link {
	color: black;
	text-decoration: none;
}

a:visited {
	color: black;
	text-decoration: none;
}
/*  li:hover { background:orange;}  */
</style>

</head>
<body>
	<header>
		<div class="container header_secondary">
			<!-- 로고 이미지 -->
			<div class="divLeft">
				<a href="/first"> <img
					src="${pageContext.request.contextPath}/resources/images/logo.png"
					style="width: 100px; height: 100px;">
				</a>
			</div>
			<!-- End of Logo Image -->

			<div class="divRight">
				<nav>
					<ul class="nav navbar-nav navbar-right secondary-nav">
						<li><a href="">로그인</a></li>
						<li><a href="signUp">회원가입</a></li>
						<li><a href="">Help Center</a></li>
						<li><a href="">FAQ</a></li>
					</ul>
				</nav>
			</div>
		</div>

	</header>

	<div class="signUpForm">
		<form action="signUpCustomer" method="post">
			<div class="signLDIV" style="line-height: 0.7em;">
				<br> <font style="font-size: 18px">아이디</font> <br> <br>
				<br> <br> <br> <br> <font
					style="font-size: 18px">이름</font> <br> <br> <br> <br>
				<br> <br> <font style="font-size: 18px">비밀번호</font> <br>
				<br> <br> <br> <br> <br> <font
					style="font-size: 18px">비밀번호 확인</font><br> <br> <br>
				<br> <br> <br> <font style="font-size: 18px">이메일</font><br>
				<br> <br> <br> <br> <br> <font
					style="font-size: 18px">성별</font><br> <br> 
				<br> <br> <br> <font style="font-size: 18px">핸드폰 번호</font><br>
				<br> <br> <br> <br> <br> <font
					style="font-size: 18px">주소</font> <br> <br> <br> <br>
				<br> <br> <br> <br> <br> <br> <br>
				<br> <br> <br> <br> <br> <br> <br><br>
				<br> <font style="font-size: 18px">관심분야</font>
			</div>
			<div class="signRDIV" style="line-height: 0.3em;">
		<!-- 아이디 -->	
				<input type="text" class="form-control" name="" id="" autofocus>
				 <br><br><br><br><br><br> 
		
		<!-- 이름 -->		 
				<input type="text" class="form-control" name="" id="" > 
				<br><br><br><br><br><br> 
				
		<!-- 비밀번호 -->		
				<input type="text" class="form-control" id="" >
				<br><br><br><br><br><br> 
			
		<!-- 비밀번호 확인 -->	
				<input type="text" class="form-control" name="" id="">
				<br><br><br><br><br><br> 
		<!-- 이메일 -->		
				<input type="text" class="form-control" name="" id="" >
				<br><br><br><br><br><br><br>
				
		<!-- 성별 -->
				<input type="radio">
				<font style="font-size: 20px;">남자</font>&nbsp;&nbsp;&nbsp;
				<input type="radio">
				<font style="font-size: 20px;">여자</font>
				<br><br><br><br><br><br><br><br><br><br><br>
				
		<!-- 핸드폰번호 -->		
				<input type="text" class="form-control" name="" id="" autofocus>
				<br><br><br><br><br><br>
				
		<!-- 우편번호 -->			
				<div style="width: 50%; float: left;">
				<input type="text" id="sample6_postcode" placeholder="우편번호" class="form-control" style="width: 150px;">
				</div>
				<div style="width: 30%; float: right;">
					<input type="button" onclick="sample6_execDaumPostcode()"
						class="form-control btn btn-primary signupbtn" value="우편번호 찾기"
						style="width: 100px;">
				</div>
				<br> <br> <br> <br> <br> <br> <input
					class="form-control" type="text" id="sample6_address"
					placeholder="주소"><br> <br> <input type="text"
					class="form-control" id="sample6_detailAddress" placeholder="상세주소"><br>
				<br> <br> <br> <br> <br> <input type="text"
					class="form-control" id="sample6_extraAddress" placeholder="참고항목">
				<br> <br> <br> <br> <br> <br> 
				
			<!-- 관심분야  -->
				<select class="form-control">
					<option>관심분야1
					<option>일본어
					<option>영어
				</select> 
				<br><br><br><br><br><br> 
				
				<select class="form-control">
					<option>관심분야					
					<option>2일본어
					<option>영어
				</select>
				<br><br><br><br><br>

			<!-- 회운가입버튼 -->
				<button type="submit" class="form-control btn btn-primary signupbtn" style="background-color: #286090">
					<font style="color: white;">회원가입</font>
				</button>


			</div>


		</form>
	</div>
<!-- 아래여백 -->
	<div style="height: 300px;"></div>
</body>
</html>