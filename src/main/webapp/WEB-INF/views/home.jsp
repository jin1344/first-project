<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>Home</title>

<link rel='stylesheet' id='ticketlab-helpers-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/helpers.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel='stylesheet' id='main-bbt-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/bbt.css?ver=4.9.9'
	type='text/css' media='all' />

<style>

/* 상단 부분 스타일 */
.container {
	width: 100%;
	height: 15%;
	margin: 0px auto;
}

.divLeft {
	width: 15%;
	height: 70%;
	float: left;
	position: relative;
	left: 40px;
}

.divRight {
	width: 80%;
	height: 70%;
	float: right;
}

/* 상단 폰트 설정 */
.header_secondary .navbar-nav>li>a {
	color: #fff;
	font-size: 15px;
}

.header_secondary {
	background-color: #000 !important;
}

.header_secondary .second_menu {
	color: #fff !important;
}

.header_secondary .navbar-brand {
	padding-top: 15px !important
}

.title-center {
	position: relative;
	left: 43%;
}

.nav-title {
	font-size: 30px;
	/* 	color: blue; */
	list-style: none;
}

.liSet {
	font: strong 30px Dotum blue;
	padding: 20px 15px;
	font-style:
}

/* nav tag */
nav ul {
	padding-top: 10px;
} /* 상단 여백 10px */
nav ul li {
	display: inline; /* 세로나열을 가로나열로 변경 */
	border-left: 1px solid #999; /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
	font: bold 12px Dotum; /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
	padding: 0 10px; /* 각 메뉴 간격 */
}

nav ul li:first-child {
	border-left: none;
}

/
* 메뉴 분류중 제일 왼쪽의 "|"는 삭제


.root {
	width: 100%;
	height: 800px;
	background-color: #fff
}

.titleDiv {
	height: 20%;
	margin: 0px auto;
	border: 1px solid #bcbcbc;
}

.login {
	margin-left: 80%;
}

.divCenter {
	width: 66%;
	height: 70%;
	float: left;
}

.divRight {
	width: 40%;
	height: 70%;
	float: right;
}

.auth {w
	
}

.bar {
	letter-spacing: 10px;
}
</style>



<style type="text/css">
body {
	margin: 0;
	padding: 0;
}

a:link {
	color: black;
	text-decoration: none;
}

a:visited {
	color: black;
	text-decoration: none;
}
table {
	border: 0;
}
/*  li:hover { background:orange;}  */
</style>


</head>

<body>
	<header>
		<div class="container header_secondary">
			<!-- 로고 이미지 -->
			<div class="divLeft">
				<a href="/first"> <img
					src="${pageContext.request.contextPath}/resources/images/logo.png"
					style="width: 100px; height: 100px;">
				</a>
			</div>
			<!-- End of Logo Image -->

			<div class="divRight">
				<nav>
					<ul class="nav navbar-nav navbar-right secondary-nav">
						<li><a href="">로그인</a></li>
						<li><a href="signUp">회원가입</a></li>
						<li><a href="">Help Center</a></li>
						<li><a href="">FAQ</a></li>
					</ul>
				</nav>
			</div>
		</div>

	</header>





	<div id="" class="bg-welcome">
		<div class="welcome">
			<div class="container">
				<div class="title-center">

					<ul class="navbar-nav nav-title ">
						<li class="liSet"><a href=""><span style="color: orange;"></span></a></li>
						<li class="liSet"><a href=""><span>로그인</span></a></li>
						<li class="liSet"><a href=""><span>로그인</span></a></li>
						<li class="liSet"><a href=""><span>로그인</span></a></li>
					</ul>

				</div>

				<h1>Shall we Study?</h1>
				<h2>찾으시는 스터디를 검색</h2>

				<form class="" action="" method="post">
					<div class="form-group group-search">
						<input class="form-control search-btn" name="s"
							placeholder="Search" type="text">
						<!-- // 히든 서치벨류값-->
						<input type="hidden" name="post_type" value="searchValue" />
					</div>


					<button type="submit" class="btn btn-default btn-search"
						style="position: relative; bottom: 2.43px;">
						<!-- <i class="fa fa-search"></i> -->
					</button>
				</form>


			</div>
			<!-- /.welcome -->
		</div>
	</div>
	<table>
		<tr>
			<td>asd</td>
			<td>ddd</td>
		</tr>
	</table>







</body>
</html>
