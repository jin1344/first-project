<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Shall We Study</title>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel='stylesheet' id='ticketlab-helpers-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/helpers.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel='stylesheet' id='main-bbt-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/bbt.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">



<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!--캘린더 부분 스타일  -->
 <style type="text/css">
.cal_top{
    text-align: center;
    font-size: 30px;
}
.cal{
    text-align: center;    
}
table.calendar{
    border: 1px solid black;
    display: inline-table;
    text-align: left;
}
table.calendar td{
    vertical-align: top;
    border: 1px solid skyblue;
    width: 100px;
}
</style>


<!-- 해더 부분 스타일 -->
<style>

/* 상단 부분 스타일 */
	.container {
	width: 100%;
	height: 15%;
	margin: 0px auto;
	}

	.divLeft {
	width: 15%;
	height: 70%;
	float: left;
	position: relative;
	left: 40px;
	}

	.divRight {
	width: 80%;
	height: 70%;
	float: right;
	}

/* 상단 폰트 설정 */
	.header_secondary .navbar-nav>li>a {
	color: #fff;
	font-size: 15px;
	}

	.header_secondary {
	background-color: #000 !important;
	}

	.header_secondary .second_menu {
	color: #fff !important;
	}

	.header_secondary .navbar-brand {
	padding-top: 15px !important
	}	

	.title-center {
	position: relative;
	left: 43%;
	}

	.nav-title {
	font-size: 30px;
	/* 	color: blue; */
	list-style: none;
	}

	.liSet {
	font: strong 30px Dotum blue;
	padding: 20px 15px;
	font-style:
	}

/* nav tag */
	nav ul {
	padding-top: 10px;
	} /* 상단 여백 10px */
	nav ul li {
	display: inline; /* 세로나열을 가로나열로 변경 */
	border-left: 1px solid #999; /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
	font: bold 12px Dotum; /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
	padding: 0 10px; /* 각 메뉴 간격 */
	}

	nav ul li:first-child {
	border-left: none;
	}

	/
	* 메뉴 분류중 제일 왼쪽의 "|"는 삭제


	.root {
	width: 100%;
	height: 800px;
	background-color: #fff
	}

	.titleDiv {
	height: 20%;
	margin: 0px auto;
	border: 1px solid #bcbcbc;
	}

	.login {
	margin-left: 80%;
	}

	.divCenter {
	width: 66%;
	height: 70%;
	float: left;
	}

	.divRight {
	width: 40%;
	height: 70%;
	float: right;
	}

	.auth {w
	
	}

	.bar {
	letter-spacing: 10px;
	}
</style>

<style type="text/css">
body {
	margin: 0;
	padding: 0;
}

/* a:link {
	color: black;
	text-decoration: none;
}

a:visited {
	color: black;
	text-decoration: none;
} */
table {
	border: 0;
}
/*  li:hover { background:orange;}  */
</style>



</head>
<body>

<!-- 해더부분 -->
<header> 
		<div class="container header_secondary">
			<!-- 로고 이미지 -->
			<div class="divLeft">
				<a href="/first"> <img
					src="${pageContext.request.contextPath}/resources/images/logo.png"
					style="width: 100px; height: 100px;">
				</a>
			</div>
			<!-- End of Logo Image -->

			<div class="divRight">
				<nav>
					<ul class="nav navbar-nav navbar-right secondary-nav">
						<li><a href="">로그인</a></li>
						<li><a href="signUp">회원가입</a></li>
						<li><a href="">Help Center</a></li>
						<li><a href="">FAQ</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>


<h1 align="center" >도전 첼린저</h1>


<!--달력부분 -->
    <div class="cal_top">
        <a href="#" id="movePrevMonth" class="btn btn-default" role="button"><span id="prevMonth" class="cal_tit">&lt;</span></a>
        <span id="cal_top_year"></span>
        <span id="cal_top_month"></span>
        <a href="#" id="moveNextMonth" class="btn btn-default" role="button"><span id="nextMonth" class="cal_tit">&gt;</span></a>
    </div>
    <div id="cal_tab" class="cal">
    </div>
 
 
 <!--켈린더부분 스크립  -->
<script>
    
    var today = null;
    var year = null;
    var month = null;
    var firstDay = null;
    var lastDay = null;
    var $tdDay = null;
    var $tdSche = null;
 
    $(document).ready(function() {
    	
    	
    	
     	drawCalendar(); 
        initDate(); 
        drawDays(); 
        drawSchedules();  
       
              
        $("#movePrevMonth").on("click", function(){movePrevMonth();});
        $("#moveNextMonth").on("click", function(){moveNextMonth();});
    });
    
    //calendar 그리기
    function drawCalendar(resp){
        var setTableHTML = "";
    	setTableHTML+='<table class="calendar">'; 
        setTableHTML+='<tr><th>SUN</th><th>MON</th><th>TUE</th><th>WED</th><th>THU</th><th>FRI</th><th>SAT</th></tr>';
        for(var i=0;i<6;i++){
            setTableHTML+='<tr height="100">';
            for(var j=0;j<7;j++){
                setTableHTML+='<td style="text-overflow:ellipsis;overflow:hidden;white-space:nowrap">';
                setTableHTML+='    <div class="cal-day"></div>';
               	setTableHTML+='    <div class="cal-schedule"></div>'; 
                setTableHTML+='</td>';
            }
            setTableHTML+='</tr>';
        }
        setTableHTML+='</table>';
        $("#cal_tab").html(setTableHTML);
        
    }
 
    //날짜 초기화
    function initDate(){
        $tdDay = $("td div.cal-day")
        $tdSche = $("td div.cal-schedule")
        dayCount = 0;
        today = new Date();
        year = today.getFullYear();
        month = today.getMonth()+1;
        firstDay = new Date(year,month-1,1);
        lastDay = new Date(year,month,0);
    }
    
    //calendar 날짜표시
    function drawDays(){
        $("#cal_top_year").text(year);
        $("#cal_top_month").text(month);
        for(var i=firstDay.getDay();i<firstDay.getDay()+lastDay.getDate();i++){
            $tdDay.eq(i).text(++dayCount);
        }
        for(var i=0;i<42;i+=7){
            $tdDay.eq(i).css("color","red");
        }
        for(var i=6;i<42;i+=7){
            $tdDay.eq(i).css("color","blue");
        }
        
        
    }
 	
  //calendar 스케쥴 표시
    function drawSchedules(){
       /*   year = $("#cal_top_year").text();
        month = $("#cal_top_month").text();  */
      /*   if(month<10){
            month=String("0"+month);
        }  */
        var now = year + "-" + month;
        
        
        $.ajax({
       		url:"challengeList",
       		data: {
       			now : now
       		},
       		type:"get",
       		success : function(resp) {
       			
		         	  $.each(resp, function(index, item){
		         		// String에서 뒤에서부터 '-' 있는 곳까지 위치를 찾아주는 함수
		         		var date = item.examdate.lastIndexOf('-');
		         		// 뒤에서부터 그 위치까지만 글자를 잘라내는 것
		         		var realDate = item.examdate.substring(date+1);
		         		
		         		var challengeName =item.name;
		         		
		         		/* $('.cal-day').filter(function(){
		                    return $(this).text() === realDate ;
		              }).next().html('<a href="">'+challengeName+'</a>');  */
		            var daySearch = $('.cal-day').filter(function(){
		                    return $(this).text() === realDate ;
		              });
		              daySearch.next().append('<a  href="detailChallenge?examseq='+item.examseq+'">'+challengeName+'</a><br>');
		        	});  
				} 
       	});
    } 
    
   /*  class="btn btn-default" role="button" */
  
    //calendar 월 이동
    function movePrevMonth(){
        month--;
        if(month<=0){
            month=12;
            year--;
        }
      /*   if(month<10){
            month=String("0"+month);
        }  */
        
        
        getNewInfo();
        drawSchedules();
        }
    
    function moveNextMonth(){
        month++;
        if(month>12){
            month=1;
            year++;
        }
       /*   if(month<10){
            month=String("0"+month);
        }  */
     

        getNewInfo();
        drawSchedules();
       }

    
    function getNewInfo(){
        for(var i=0;i<42;i++){
            $tdDay.eq(i).text("");
            $tdSche.eq(i).text("");
        }
        dayCount=0;
        firstDay = new Date(year,month-1,1);
        lastDay = new Date(year,month,0);
        drawDays();
        
    }
</script>


</body>
</html>