<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel='stylesheet' id='ticketlab-helpers-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/helpers.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel='stylesheet' id='main-bbt-css-css'
	href='https://support.kollus.com/ko/wp-content/themes/ticketlab/css/bbt.css?ver=4.9.9'
	type='text/css' media='all' />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">


<meta charset="UTF-8">
<title>쉘위스터디</title>

<!-- 셀랙트박스 스타일  -->
<style type="text/css">
select { -webkit-appearance: none; /* 네이티브 외형 감추기 */
 -moz-appearance: none;
  appearance: none; 
  background: url(이미지 경로) no-repeat 95% 50%; /* 화살표 모양의 이미지 */ 
  } 
  /* IE 10, 11의 네이티브 화살표 숨기기 */ 
  select::-ms-expand { display: none; }

select { width: 200px; /* 원하는 너비설정 */ padding: .8em .5em; /* 여백으로 높이 설정 */ font-family: inherit; /* 폰트 상속 */ background: url(https://farm1.staticflickr.com/379/19928272501_4ef877c265_t.jpg) no-repeat 95% 50%; /* 네이티브 화살표 대체 */ border: 1px solid #999; border-radius: 0px; /* iOS 둥근모서리 제거 */ -webkit-appearance: none; /* 네이티브 외형 감추기 */ -moz-appearance: none; appearance: none; }
</style>

<!-- 해더부분 스타일  -->
<style>

/* 상단 부분 스타일 */
 	.container {
	width: 100%;
	height: 15%;
	margin: 0px auto;
	} 

	.divLeft {
	width: 15%;
	height: 70%;
	float: left;
	position: relative;
	left: 40px;
	}

	.divRight {
	width: 80%;
	height: 70%;
	float: right;
	}

/* 상단 폰트 설정 */
	.header_secondary .navbar-nav>li>a {
	color: #fff;
	font-size: 15px;
	}

	.header_secondary {
	background-color: #000 !important;
	}

	.header_secondary .second_menu {
	color: #fff !important;
	}

	.header_secondary .navbar-brand {
	padding-top: 15px !important
	}	

	.title-center {
	position: relative;
	left: 43%;
	}

	.nav-title {
	font-size: 30px;
	/* 	color: blue; */
	list-style: none;
	}

	.liSet {
	font: strong 30px Dotum blue;
	padding: 20px 15px;
	font-style:
	}

/* nav tag */
	nav ul {
	padding-top: 10px;
	} /* 상단 여백 10px */
	nav ul li {
	display: inline; /* 세로나열을 가로나열로 변경 */
	border-left: 1px solid #999; /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
	font: bold 12px Dotum; /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
	padding: 0 10px; /* 각 메뉴 간격 */
	}

	nav ul li:first-child {
	border-left: none;
	}

	/
	* 메뉴 분류중 제일 왼쪽의 "|"는 삭제


	.root {
	width: 100%;
	height: 800px;
	background-color: #fff
	}

	.titleDiv {
	height: 20%;
	margin: 0px auto;
	border: 1px solid #bcbcbc;
	}

	.login {
	margin-left: 80%;
	}

	.divCenter {
	width: 66%;
	height: 70%;
	float: left;
	}

	.divRight {
	width: 40%;
	height: 70%;
	float: right;
	}

	.auth {w
	
	}

	.bar {
	letter-spacing: 10px;
	}
</style>

<style type="text/css">
body {
	margin: 0;
	padding: 0;
}

a:link {
	color: black;
	text-decoration: none;
}

a:visited {
	color: black;
	text-decoration: none;
}
table {
	border: 0;
}
/*  li:hover { background:orange;}  */
</style>

<!-- <style type="text/css">

 dl {
  display: block;
  margin-top: 1em;
  margin-bottom: 1em;
  margin-left: 0;
  margin-right: 0;
}
 
</style> -->


<!-- 어디선가 가져온 디테일 CSS -->
<style type="text/css">

 @import 'https://fonts.googleapis.com/css?family=Open+Sans|Roboto:300'; 

$padding:30px;

* { box-sizing: border-box; }

body { background: #757575; overflow-x: hidden; }
.container2 {
  perspective: 800px;
  
  /* Styling */  
  color: #fff;
  font-family: 'Open Sans', sans-serif;
  text-transform: uppercase;
  letter-spacing: 4px;
  
  /* Center it */
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
.card {
  /* Styling */
  width: 700px;
  height: 400px;
  background: rgb(20,20,20);
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);  
  
  /* Card flipping effects */
  transform-style: preserve-3d;
  transition: 0.6s; 
}
.side {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
  /* Fix Chrome rendering bug */
  transform: rotate(0deg) translateZ(1px);
}

/* Flip the card on hover */
.container2:hover .card,
.back {
  transform: rotateY(-180deg) translateZ(1px);
}

/* Front styling */
.front {
  /* Center the name + outline (almost) */
  line-height: 390px; /* Height - some (because visual center is a little higher than actual center) */
  text-align: center;
}
.logo {
  outline: 1px solid #19F6E8;
  display: inline-block;
  padding: 15px 40px;
  
  text-transform: uppercase;
  font-family: 'Roboto', sans-serif;
  font-size: 1.4em;
  font-weight: normal;
  line-height: 32px;
  letter-spacing: 8px;
}

/* Back styling */
.back {
  background: #15CCC0;
  padding: $padding;
}
.name {
  color: #3B3B3B;
  margin-bottom: 0;
  text-align:center;
}
p {
  margin: 0.8em 0;
  
}
.info {
  position: absolute;
  bottom: $padding;
  left: $padding;
  color: #3b3b3b;
  top: 120px;
  right: 200px;
  font-size: 13.5pt;
}
.property {
  color: #fff;
  
}
.description{
 position: absolute;
  color: #fff;
  top: 80px;
  right: 300px;
  font-size: 13.5pt;
}

/* Make semi-responsive */
@media (max-width:700px) {
  .card { transform: scale(.5); }
  .container2:hover .card { transform: scale(.5) rotateY(-180deg) translateZ(1px); }
}

</style>





<script>

$(function(){
	$('#challenge').on('click',check);
});

 function check(){
	 var checkMoney = $('#selectMoney').val();
	 
	 if(checkMoney=="money"){
		 alert("금액을 선택해 주세요");
		 return;
	 }
	 
	$('#goChallenge').submit();
	
	 
 }


</script>

</head>
<body>

<!-- 해더부분 -->
<header> 
		<div class="container header_secondary">
			<!-- 로고 이미지 -->
			<div class="divLeft">
				<a href="/first"> <img
					src="${pageContext.request.contextPath}/resources/images/logo.png"
					style="width: 100px; height: 100px;">
				</a>
			</div>
			<!-- End of Logo Image -->

			<div class="divRight">
				<nav>
					<ul class="nav navbar-nav navbar-right secondary-nav">
						<li><a href="">로그인</a></li>
						<li><a href="signUp">회원가입</a></li>
						<li><a href="">Help Center</a></li>
						<li><a href="">FAQ</a></li>
					</ul>
				</nav>
			</div>
		</div>
</header>


<%-- <div align="center">
<dl>
<dt>${exam.name}</dt>
<dd>시험 일정 : ${exam.examdate}</dd>
<dd>응 시 료 : ${exam.invoice} 원</dd>
</dl>
</div>
<hr>
<div align="center">
<form id="goChallenge" action="entryChallenge" method="post">
<input type="hidden" name="examseq" value="${exam.examseq}"/>

<!--로그인한 유저의 아이디값을 받아오기위해 만듬. 테스트때는 주석처리함 -->
<input type="hidden" name="userid" value="${sessionScope.xxxxxxx}"/>
<select id="selectMoney" name="entryfee" style="font-size:12pt">
	<option value="money">금액선택</option>
	<option value="1000">1000원</option>
	<option value="2000">2000원</option>
	<option value="3000"> 3000원</option>
	<option value="5000">5000원</option>
	<option value="10000">10000원</option>
</select>
	<input class="btn btn-primary btn-lg" id="challenge" type="button" value="도전"/>
</form> 
</div> --%>



<div class="container2">
  <div class="card">
    
    <div class="front side">
      <h1 class="logo">${exam.name}</h1>
    </div>
    
    <div class="back side">
      <h3 class="name">${exam.name}</h3>
      <div><span class="description">도전금을 내고 성공하면 어쩌고 저쩌고</span></div>      
      <div class="info">
        <p><span class="property">시험 일정: </span>${exam.examdate}</p>
        <p><span class="property">응 시 료: </span>${exam.invoice} 원</p>
        <p><span class="property">챌린저마감일 </span>시험전날까지</p>
        <form id="goChallenge" action="entryChallenge" method="post">
        <select id="selectMoney" name="entryfee" style="font-size:12pt">
			<option value="money">금액선택</option>
			<option value="1000">1000원</option>
			<option value="2000">2000원</option>
			<option value="3000"> 3000원</option>
			<option value="5000">5000원</option>
			<option value="10000">10000원</option>
		</select>
			<input class="btn btn-primary btn-lg" id="challenge" type="button" value="도전"/>
        </form> 
      </div>
    </div>
    
  </div>
</div>



</body>
</html>